﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Threading;
using System.Net.Sockets;

namespace np_lesson3
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TcpClient tcpClient = new TcpClient();
        public MainWindow()
        {
            InitializeComponent();
            textBoxUserName.Text = System.Environment.MachineName.ToString();
        }

        private void ButtonConnectClick(object sender, RoutedEventArgs e)
        {
            try
            {
                tcpClient.Connect(IPAddress.Parse(textBoxServerIp.Text), int.Parse(textBoxPort.Text));
                textBoxIncome.AppendText(tcpClient.Connected.ToString() + "\n");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
